<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/trippleButton.css">

<title>ユーザ一覧</title>
</head>
<body>
<div class="container">
  <header>
    <nav class="navbar navbar-dark sticky-top bg-primary flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">${sessionScope.userInfo.name}　さん</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>


		<br>
			<h1 align="center">ユーザ一覧</h1>
      <div class="text-right">
        <a href="userAddServlet">新規登録</a>
      </div>

		<form action="userListServlet" method="post">
			<div class="row">

				<div class="col-4" align="right">ログインID</div>
				<div class="col-5">
					<div class="form-group">
						<input type="text" class="form-control" id="exampleInputId1"
							placeholder="ログインID" name="loginId">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-4" align="right">ユーザ名</div>
				<div class="col-5">
					<div class="form-group">
						<input type="text" class="form-control"
							id="exampleInputPassword1" placeholder="ユーザ名" name="name">
					</div>
					<div class="form-group form-check"></div>
				</div>
			</div>

			<div class="row">

				<div class="col-4" align="right">生年月日</div>

				<div class="col-2">
					<div class="form-group">
						<input type="date" class="form-control" id="exampleInputEmail1"
							placeholder="生年月日" name="StBirthDate">
					</div>
				</div>

				<div class="col-1" align="center">
					<h2>～</h2>
				</div>

				<div class="col-2">
					<div class="form-group">
						<input type="date" class="form-control" id="exampleInputEmail1"
							placeholder="生年月日" name="EdBirthDate">
					</div>
				</div>

			</div>
			<div align="center">
				<button type="submit" class="btn btn-primary">検索</button>
			</div>
			<br>
		</form>
	</div>




	<div class="container">
		<table class="table">


			<thead class="table-bordered">
				<tr>
					<th scope="col">ログインID</th>
					<th scope="col">ユーザ名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>

			</thead>
			<tbody>
			 <c:forEach var="list" items="${userList}">
				<tr>
					<th scope="row">${list.loginId}</th>
					<td>${list.name}</td>
					<td>${list.birthDate}</td>
                     <td>

                       <a class="btn btn-primary" href="UserDetailServlet?id=${list.id}">詳細</a>

					<c:choose>
                       <c:when test="${userInfo.loginId == 'admin'}">
                       	<a class="btn btn-success" href="UserUpdateServlet?id=${list.id}">更新</a>
                       </c:when>

                       <c:otherwise>
                       	<c:if test="${userInfo.loginId == list.loginId}">
                       	<a class="btn btn-success" href="UserUpdateServlet?id=${list.id}">更新</a>
                       	</c:if>
                       </c:otherwise>
                     </c:choose>

                       <c:if test="${userInfo.loginId =='admin'}">
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${list.id}">削除</a>
                       </c:if>

                     </td>
				</tr>
			</c:forEach>
				<tr>
			</tbody>
		</table>
	</div>


</body>
</html>