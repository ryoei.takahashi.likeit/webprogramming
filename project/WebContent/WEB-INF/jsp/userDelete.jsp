<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
</head>
<body>

	<div class="container">
		<header>
			<nav class="navbar navbar-dark sticky-top bg-primary flex-md-nowrap p-0">
				<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">${sessionScope.userInfo.name}　さん</a>
				<ul class="navbar-nav px-3">
					<li class="nav-item text-nowrap"><a class="nav-link" href="LogoutServlet">ログアウト</a>
					</li>
				</ul>
			</nav>
		</header>

		<h1 align="center">ユーザ削除確認</h1>
		<br>
		<p align="center">
			ログインID:<c:out value="${user.loginId}"/><br> を本当に削除してよろしいでしょうか。
		</p>
		<br> <br>

		<div align="center">
			<form action="UserDeleteServlet" method="post">
			<a class="btn btn-primary" href ="userListServlet">キャンセル</a>
			<input type="hidden" name="id" value="${user.id}">
			<input type="submit" class="btn btn-primary" value="OK">
			</form>
		</div>
	</div>
</body>
</html>