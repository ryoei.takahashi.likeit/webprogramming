<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
</head>
<body>
<div class="container">
		<header>
			<nav class="navbar navbar-dark sticky-top bg-primary flex-md-nowrap p-0">
				<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">${sessionScope.userInfo.name}　さん</a>
				<ul class="navbar-nav px-3">
					<li class="nav-item text-nowrap"><a class="nav-link" href="LogoutServlet">ログアウト</a>
					</li>
				</ul>
			</nav>
		</header>

		<h1 align="center">ユーザ情報詳細参照</h1>
		<br>
		<div class="container">

			<div class="row">
				<div class="col">ログインID</div>
				<div class="col"><c:out value="${user.loginId}"/></div>
				<div class="w-100"></div><br>
				<div class="col">ユーザ名</div>
				<div class="col"><c:out value="${user.name}"/></div>
				<div class="w-100"></div><br>
				<div class="col">生年月日</div>
				<div class="col"><c:out value="${user.birthDate}"/></div>
				<div class="w-100"></div><br>
				<div class="col">登録日時</div>
				<div class="col"><c:out value="${user.createDate}"/></div>
				<div class="w-100"></div><br>
				<div class="col">更新日時</div>
				<div class="col"><c:out value="${user.updateDate}"/></div>
			</div>
			<div align="left">
			<a class="nav-link" href="userListServlet">戻る</a>
			</div>
		</div>




	</div>

</body>
</html>