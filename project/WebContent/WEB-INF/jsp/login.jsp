<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>ログイン画面</title>
</head>
<body>
	<div class="container">
		<br>
<div class="p-3 mb-2 bg-primary text-white">
			<h1 align="center">ログイン画面</h1></div>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
		<br>

		<div align="center">
			<form action="loginServlet" method="post">
				<div class="row">

					<div class="col-4" align="right">ログインID</div>
					<div class="col-5">
						<div class="form-group">
							<input type="text" name="loginId" class="form-control" id="Inputid"
								placeholder="ログインID">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-4" align="right">パスワード</div>
					<div class="col-5">
						<div class="form-group">
							<input type="password" name="password" class="form-control"
								id="InputPassword1" placeholder="パスワード">
						</div>
						<div class="form-group form-check"></div>
					</div>
				</div>
				<div align="center">
					<button type="submit" class="btn btn-primary">ログイン</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>