<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<meta charset="UTF-8">
<title>ユーザ新規登録画面</title>
</head>
<body>
	<div class="container">
		<header>
			<nav class="navbar navbar-dark sticky-top bg-primary flex-md-nowrap p-0">
				<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">${sessionScope.userInfo.name}　さん</a>
				<ul class="navbar-nav px-3">
					<li class="nav-item text-nowrap"><a class="nav-link" href="LogoutServlet">ログアウト</a>
					</li>
				</ul>
			</nav>
		</header>
			<h1 align="center">ユーザ新規登録</h1>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

		<br> <br>
		<div align="center">
			<form action="userAddServlet" method="post">
				<div class="row">

					<div class="col-4" align="right">ログインID</div>
					<div class="col-5">
						<div class="form-group">
							<input type="text" name="loginId" class="form-control" id="loginId"
								placeholder="ログインID" value="${user.loginId}">
						</div>
					</div>
				</div>
				<br>

				<div class="row">

					<div class="col-4" align="right">パスワード</div>
					<div class="col-5">
						<div class="form-group">
							<input type="password" name="password" class="form-control" id="password"
								placeholder="パスワード">
						</div>
					</div>
				</div>
				<br>

				<div class="row">

					<div class="col-4" align="right">パスワード(確認)</div>
					<div class="col-5">
						<div class="form-group">
							<input type="password" class="form-control" id="passwordConfirm" name="password2"
								placeholder="パスワード（確認）">
						</div>
					</div>
				</div>
				<br>

				<div class="row">

					<div class="col-4" align="right">ユーザ名</div>
					<div class="col-5">
						<div class="form-group">
							<input type="text" name="name" class="form-control" id="name"
								placeholder="ユーザ名" value="${user.name}">
						</div>
					</div>
				</div>
				<br>

				<div class="row">

					<div class="col-4" align="right">生年月日</div>
					<div class="col-5">
						<div class="form-group">
							<input type="date" name="birthDate" class="form-control" id="birthDate"
								placeholder="生年月日" value="${user.birthDateStr}">
						</div>
					</div>
				</div>
				<br>

				<button type="submit" class="btn btn-primary">登録</button>
			</form>
			<div align="left">
			<a class="nav-link" href="userListServlet">戻る</a>
			</div>
		</div>

</body>
</html>