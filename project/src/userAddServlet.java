
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDao;
import model.User;

/**
 * Servlet implementation class userAddServlet
 */
@WebServlet("/userAddServlet")
public class userAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public userAddServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("loginServlet");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userADD.jsp");
			dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birthDate");
		String password2 = request.getParameter("password2");

		boolean isError = false;
		// 空白チェック
		if (loginId.equals("") || password.equals("") || password2.equals("") || name.equals("")
				|| birth_date.equals("")) {
			isError = true;
		}

		// パスワード一致チェック
		if (!password.equals(password2)) {
			isError = true;
		}

		UserDao UserDao = new UserDao();
		boolean isLoginIdCheck = UserDao.findLoginId(loginId);

		// 既存ログインID一致チェック
		if (isLoginIdCheck) {
			isError = true;
		}

		if (isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			User user2 = new User();
			user2.setLoginId(loginId);
			user2.setName(name);
			user2.setBirthDate(birth_date);

			request.setAttribute("user", user2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userADD.jsp");
			dispatcher.forward(request, response);
		} else {
			UserDao userDao = new UserDao();
			userDao.insertUser(loginId, password, name, birth_date);
		}

		response.sendRedirect("userListServlet");
	}

}
