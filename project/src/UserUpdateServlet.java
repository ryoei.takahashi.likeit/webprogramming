

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();
		User userInfo =(User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("loginServlet");
			return;
		}

		String id = request.getParameter("id");

		System.out.println(id);

		UserDao UserDao = new UserDao();
		User user = UserDao.findById(id);

		request.setAttribute("user", user);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birthDate");
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String password2 = request.getParameter("password2");

		boolean isError = false;

		// 空白チェック
		if (password.equals("") || password2.equals("") || name.equals("")
				|| birth_date.equals("")) {
			isError = true;
		}

		// パスワード一致チェック
		if (!password.equals(password2)) {
			isError = true;
		}

		if(isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			User user2 = new User();
			user2.setLoginId(loginId);
			user2.setName(name);
			user2.setBirthDate(birth_date);

			request.setAttribute("user", user2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}else{
		UserDao UserDao = new UserDao();
		UserDao.UpdateUser(password, name, birth_date,id);

		response.sendRedirect("userListServlet");

		}
	}

}
